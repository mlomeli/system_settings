class Setting < ActiveRecord::Base

  default_scope { order('id') } 

  STRING = 0
  BOOLEAN = 1
  DOUBLE = 2
  PERCENT = 3
  DATE = 4
  LIST = 5
  INTEGER = 6

  def select_options
    self.options.split(",")
  end
  
  def self.number_types name
    nastring_types.index name
  end
  
  def self.string_types
     %w( integer boolean double percent date list string)
  end

  def self.set key, value
    setting = where(key:key).first_or_create
    setting.value = value
    setting.save
  end

  def self.get key
    where(key:key).first.try(:value)
  end

end
