class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.string :key
      t.string :value
      t.integer :setting_type, default:0
      t.text :options

      t.timestamps
    end
  end
end
