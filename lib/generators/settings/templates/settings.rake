namespace :settings do
  desc "Create setting"
  task :create, [:name, :tipo, :valor, :valores] => :environment do |t, args|
    if args.name.nil? or args.tipo.nil?
      puts "Use rake settings:create nombre tipo [valor] [valores]"
    else
      number_type = Setting.number_types args.tipo
      if number_type.nil?
        puts "Tipo debe ser alguno de los siguientes: #{Setting.string_types}"
      else
        Setting.create({name:args.name, settings_type:number_type, 
          value:args.value, options:args.valores})
        
      end
    end
  end

  desc "Delete setting"
  task :delete, [:key] => :environment do |t, args|
    Setting.where(key:key).delete_all
  end
    
  desc "List settings"
  task :list => :environment do
    #tp Setting.all
  end
  
end