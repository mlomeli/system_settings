# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on 'ready page:load', ->


  delete_pago = ->
    $('.delete-payment').on 'click', (event, data, status, xhr) ->
      event.preventDefault()
      btn =  $(this)
      form = btn.parents('form')

      remove = ->
        form.fadeOut "slow", () ->
          form.remove()

      if btn.attr('href') != '#'
        $.ajax
          url: btn.attr('href')
          type:'delete'
          success: remove
      else
        remove()

  delete_pago()

  delete_reference = ->
    $('.delete-reference').on 'click', (event, data, status, xhr) ->
      event.preventDefault()
      btn =  $(this)
      form = btn.parents('form')

      remove = ->
        form.fadeOut "slow", () ->
          form.remove()

      if btn.attr('href') != '#'
        $.ajax
          url: btn.attr('href')
          type:'delete'
          success: remove
      else
        remove()

  delete_reference()

  setting_form =  ->
    $('.setting-form input, .setting-form select').change ->
      $(this).parents('form').submit()

    $('.setting-form').on 'ajax:success',  (event, data, status, xhr) ->
      nombre = event.target.dataset.name
      alert "#{nombre} guardado correctamente"

      form = $(event.target)
      method = form.find("input[name=_method]")
      if method.val() == "post"
        method.val("patch")
        action = form.attr('action')
        form.attr('action', action + data.id)
        if nombre == "Tipo de pago"
          form.find(".delete-payment").attr('href', "/payment_types/#{data.id}.json")
        if nombre == "Tipo de referencia"
          form.find(".delete-reference").attr('href', "/reference_types/#{data.id}.json")
        

  setting_form()

  $("#add-payment").click (e) ->
    e.preventDefault()
    form = """
          <form accept-charset="UTF-8" action="/payment_types/" 
            class="setting-form form-inline" 
            data-name="Tipo de pago" data-remote="true" 
            id="edit_payment_type_1" 
            method="post">
            <div style="display:none">
              <input name="utf8" type="hidden" value="✓">
              <input name="_method" type="hidden" value="post">
            </div>
              <div class="row">
                <label class="col-sm-1">Nombre: </label>
                <div class="col-sm-4">
                  <input class="form-control" id="payment_type_name" name="payment_type[name]" type="text" value="">
                </div>
                <div class="col-sm-2">
                  <a class="btn btn-danger fa fa-ban delete-payment" href="#"></a>
                </div>    
              </div>
            <hr/>
          </form>
    """
    $("#pagos-content").prepend(form)
    setting_form()
    delete_pago()


  $("#add-reference").click (e) ->
    e.preventDefault()
    form = """
          <form accept-charset="UTF-8" action="/reference_types/" 
            class="setting-form form-inline" 
            data-name="Tipo de referencia" data-remote="true" 
            id="edit_reference_types_1" 
            method="post">
            <div style="display:none">
              <input name="utf8" type="hidden" value="✓">
              <input name="_method" type="hidden" value="post">
            </div>
              <div class="row">
                <label class="col-sm-1">Nombre: </label>
                <div class="col-sm-4">
                  <input class="form-control" id="reference_types_name" name="reference_type[name]" type="text" value="">
                </div>
                <div class="col-sm-2">
                  <a class="btn btn-danger fa fa-ban delete-reference" href="#"></a>
                </div>    
              </div>
            <hr/>
          </form>
    """
    $("#reference-content").prepend(form)
    setting_form()
    delete_reference()

