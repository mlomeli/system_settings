#lib/generators/system_settings/install_generator.rb
require 'rails/generators'
require 'rails/generators/migration'

module Settings
  module Generators
    class InstallGenerator < Rails::Generators::Base
      include Rails::Generators::Migration

      desc "Install the settings models"

      # Commandline options can be defined here using Thor-like options:
      class_option :my_opt, :type => :boolean, :default => false, :desc => "My Option"

      # I can later access that option using:
      # options[:my_opt]


      def self.source_root
        @source_root ||= File.join(File.dirname(__FILE__), 'templates')
      end

      def self.next_migration_number(path)
        Time.now.utc.strftime("%Y%m%d%H%M%S")
      end

      def copy_files

        Dir.mkdir('app/views/settings') unless File.directory?('app/views/settings')
        Dir.mkdir('lib/tasks') unless File.directory?('lib/tasks')

        copy_file "setting.rb", "app/models/setting.rb"
        copy_file "settings_controller.rb", "app/controllers/settings_controller.rb"
        copy_file "index.html.erb", "app/views/settings/index.html.erb"
        copy_file "settings.js.coffee", "app/assets/javascripts/settings.js.coffee"
        copy_file "settings.css.scss", "app/assets/stylesheets/settings.css.scss"
        copy_file "settings.rake", "lib/tasks/settings_tasks.rake"

        migration_template "create_settings.rb", "db/migrate/create_settings.rb"
        route "resources :settings"
      end

    end
  end
end
