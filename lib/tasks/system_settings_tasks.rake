namespace :system_settings
  desc "Instala los archivos necesarios"
  task :install do
    # Task goes here
    generate "install_generator"
    rake "db:migrate"
  end
end
