$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "system_settings/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "system_settings"
  s.version     = SystemSettings::VERSION
  s.authors     = ["Miguel Hernandez"]
  s.email       = ["mlomeli@engranedigital.com"]
  s.homepage    = "http://engranedigital.com"
  s.summary     = "SystemSettings."
  s.description = "SystemSettings."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  #s.files = `git ls-files`.split("\n")
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.1"
  #s.add_dependency "tp", "~> 0.7.0"

  s.add_development_dependency "sqlite3"
end
